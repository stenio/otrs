FROM debian:jessie

LABEL creator "Stenio Filho"
LABEL maintainer "internuvem@usp.br"

RUN useradd -u 10000 -r -d /opt/otrs/ -c 'OTRS user' -G www-data otrs
RUN mkdir /opt/otrs && chown otrs:www-data /opt/otrs

RUN sed -i 's/deb.debian.org/linorg.usp.br/g' /etc/apt/sources.list

RUN apt-get -y update && apt-get -y upgrade && apt-get -y install \
    apache2 \
    libapache2-mod-shib2 \
    dialog \
    libapache2-mod-perl2 \
    libdbd-mysql-perl \
    libtimedate-perl \
    libnet-dns-perl \
    libnet-ldap-perl \
    libio-socket-ssl-perl \
    libpdf-api2-perl \
    libdbd-mysql-perl \
    libsoap-lite-perl \
    libgd-text-perl \
    libtext-csv-xs-perl \
    libjson-xs-perl \
    libgd-graph-perl \
    libapache-dbi-perl \
    libarchive-zip-perl \
    libcrypt-eksblowfish-perl \
    libmail-imapclient-perl \
    libauthen-ntlm-perl \
    libtemplate-perl \
    libxml-libxml-perl \
    libxml-libxslt-perl \
    libyaml-libyaml-perl \
 && rm -rf /var/lib/apt/lists/*

ADD 'https://git.uspdigital.usp.br/stenio/otrs/raw/master/attribute-map.xml' '/etc/shibboleth/'
ADD 'https://git.uspdigital.usp.br/stenio/otrs/raw/master/shibboleth2-container.xml'  '/etc/shibboleth/'
ADD 'https://git.uspdigital.usp.br/stenio/otrs/raw/master/shibboleth2-container-dev.xml' '/etc/shibboleth/'

ADD 'https://git.uspdigital.usp.br/stenio/otrs/raw/master/otrs-ssl.conf' '/etc/apache2/sites-available/'
ADD 'https://git.uspdigital.usp.br/stenio/otrs/raw/master/shib-test.conf' '/etc/apache2/sites-available/'
ADD 'https://git.uspdigital.usp.br/stenio/otrs/raw/master/shib-otrs.conf' '/etc/apache2/sites-available/'

ADD 'https://git.uspdigital.usp.br/stenio/otrs/raw/master/apache2-foreground' '/usr/local/bin/'

RUN chmod +x /usr/local/bin/apache2-foreground

RUN openssl req -newkey rsa:2048 -new -x509 -days 3652 -nodes \
    -out /etc/shibboleth/sp-cert.pem \
    -keyout /etc/shibboleth/sp-key.pem \
    -subj "/C=BR/ST=SP/L=SaoPaulo/O=USP/OU=STI/CN=container.dev.internuvem.usp.br"

RUN a2enmod ssl headers && a2ensite default-ssl && a2dissite 000-default

EXPOSE 443
CMD ["apache2-foreground"]
