package Kernel::Config;

use strict;
use warnings;
use utf8;

sub Load {
    my $Self = shift;
   

    my $DBUser = "otrs_";
    my $DBPassword = "xxxxx";
    my $DBHost = '10.100.106.46';

    my $UNIDADE = 'XXXXX';
    my $UNIDADEID = '86';
    my $DOMINIO = 'xx.atendimento.usp.br';
    my $EmailADMIN= 'xxx@usp.br'; 


    #ID da unidade #sistema de despesa (pegar na Urania)
    $Self->{'SystemID'} =  $UNIDADEID; 
 
    # The database host
    $Self->{'DatabaseHost'} = $DBHost;

    # The database name
    $Self->{'Database'} = $DBUser;

    # The database user
    $Self->{'DatabaseUser'} = $DBUser;

    # The password of database user. You also can use bin/otrs.Console.pl Maint::Database::PasswordCrypt
    # for crypted passwords
    $Self->{'DatabasePw'} = $DBPassword;

    # The database DSN for MySQL ==> more: "perldoc DBD::mysql"
    $Self->{'DatabaseDSN'} = "DBI:mysql:database=$Self->{Database};host=$Self->{DatabaseHost}";

    $Self->{Home} = '/opt/otrs';
    
    # ---------------------------------------------------- #
    # insert your own config settings "here"               #
    # config settings taken from Kernel/Config/Defaults.pm #
    # ---------------------------------------------------- #
    # $Self->{SessionUseCookie} = 0;
    # $Self->{CheckMXRecord} = 0;

    # ---------------------------------------------------- #
     

    # Personalizacao da pagina

    $Self->{'Loader::Customer::Skin'}->{'000-default'} =  {
	  'Description' => "Página de atendimento da $UNIDADE",
	  'HomePage' => $DOMINIO,
	  'InternalName' => 'default',
	  'VisibleName' => 'Default'
	};

    $Self->{'Loader::Agent::Skin'}->{'000-default'} =  {
	  'Description' => "Página de atendimento da $UNIDADE",
	  'HomePage' => $DOMINIO,
	  'InternalName' => 'default',
	  'VisibleName' => 'Default'
	};
     
    $Self->{'CustomerHeadline'} =  "Atendimento $UNIDADE";
    $Self->{'DefaultLanguage'} =  'pt_BR';
    $Self->{'Organization'} =  $UNIDADE;
    $Self->{'AdminEmail'} =  $EmailADMIN;
    $Self->{'FQDN'} =  $DOMINIO;

 
    $Self->{'LogModule::LogFile'} =  '/opt/otrs/var/log/otrs.log';
    $Self->{'LogModule'} =  'Kernel::System::Log::File';
    $Self->{'AgentLogo'} =  {
	  'StyleHeight' => '80px',
	  'StyleRight' => '20px',
	  'StyleTop' => '25px',
	  'StyleWidth' => '480px',
	  'URL' => 'skins/Agent/default/css/thirdparty/usp/usp_logo.png'
	};
    $Self->{'CustomerLogo'} =  {
	  'StyleHeight' => '50px',
	  'StyleRight' => '25px',
	  'StyleTop' => '15px',
	  'StyleWidth' => '480px',
	  'URL' => 'skins/Customer/default/css/thirdparty/usp/usp_logo.png'
	};

     $Self->{'Loader::Customer::CommonCSS'}->{'000-Framework'} =  [
	  'Core.Reset.css',
	  'Core.Default.css',
	  'Core.Form.css',
	  'Core.Dialog.css',
	  'Core.Tooltip.css',
	  'Core.Login.css',
	  'Core.Control.css',
	  'Core.Table.css',
	  'Core.TicketZoom.css',
	  'Core.InputFields.css',
	  'Core.Print.css',
	  'thirdparty/fontawesome/font-awesome.css',
	  'thirdparty/usp/Custom.css'
	];

     $Self->{'Loader::Agent::CommonCSS'}->{'000-Framework'} =  [
	  'Core.Reset.css',
	  'Core.Default.css',
	  'Core.Header.css',
	  'Core.OverviewControl.css',
	  'Core.OverviewSmall.css',
	  'Core.OverviewMedium.css',
	  'Core.OverviewLarge.css',
  	  'Core.Footer.css',
	  'Core.PageLayout.css',
	  'Core.Form.css',
	  'Core.Table.css',
	  'Core.Widget.css',
	  'Core.WidgetMenu.css',
	  'Core.TicketDetail.css',
	  'Core.Tooltip.css',
 	  'Core.Dialog.css',
	  'Core.InputFields.css',
	  'Core.Print.css',
	  'thirdparty/fontawesome/font-awesome.css',
	  'thirdparty/usp/Custom.css'
	];

    # Agents
    $Self->{'AuthModule'} = 'Kernel::System::Auth::HTTPBasicAuth';

    # Customer Auth
    $Self->{'Customer::AuthModule'} = 'Kernel::System::CustomerAuth::HTTPBasicAuthShib';

    # Uncomment to override the environment vars to be used
    $Self->{'Customer::AuthModule::HTTPBasicAuthShib::MailEnvVar'} = 'mail';
    $Self->{'Customer::AuthModule::HTTPBasicAuthShib::FirstNameEnvVar'} = 'givenName';
    $Self->{'Customer::AuthModule::HTTPBasicAuthShib::LastNameEnvVar'} = 'sn';
    $Self->{'Customer::AuthModule::HTTPBasicAuthShib::CustomerIDEnvVar'} = 'uid';

    $Self->{LogoutURL} = 'https://idpcafe.usp.br/idp/profile/Logout';

    $Self->{CustomerPanelLogoutURL} = 'https://idpcafe.usp.br/idp/profile/Logout';
   
    $Self->{Product} = 'Seção de Computação como Serviço - CeTI-SP';

    # ---------------------------------------------------- #
    # data inserted by installer                           #
    # ---------------------------------------------------- #
    # $DIBI$

    # ---------------------------------------------------- #
    # ---------------------------------------------------- #
    #                                                      #
    # end of your own config options!!!                    #
    #                                                      #
    # ---------------------------------------------------- #
    # ---------------------------------------------------- #
}

# ---------------------------------------------------- #
# needed system stuff (don't edit this)                #
# ---------------------------------------------------- #

use Kernel::Config::Defaults; # import Translatable()
use base qw(Kernel::Config::Defaults);

# -----------------------------------------------------#

1;
